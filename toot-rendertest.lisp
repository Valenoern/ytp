;;; bop-toots.lisp - test of converting activitypub objects to bop pages {{{
(asdf:load-system "bopwiki") (asdf:load-system "bop-toots")
(asdf:load-system "bop-debug")

(defpackage :ytp-neo-temp
	(:use :common-lisp :bop-toots
		:bop-debug
	)
	(:local-nicknames  (:bop :bopwiki)) ;
	;(:export  #:bar)
)
(in-package :ytp-neo-temp)

;;; }}}


(defun main ()
	(let ((args (uiop:command-line-arguments)) stem-dir)
	(setq stem-dir (nth 1 args))  ; get $1 argument
	
	(setq stem-dir (uiop:ensure-pathname stem-dir))

	(bop:list-pages stem-dir)
))

(main)
