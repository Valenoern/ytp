;;; ytp.lisp - prerender a user stylesheet based on a list of urls {{{
; rewrite of 'ytp-neo 2020-12', its initial js version

(defpackage :ytp
	(:use :common-lisp)
	;;(:local-nicknames
		;; (:uiop :uiop)
	;;)
	(:export
		#:open-string
		#:style-rule #:style-rule-selector #:style-rule-properties #:style-rule-comment #:make-style-rule
		#:style-from-arg
))
(in-package :ytp)

;; BUG: I'm not sure how to tell sbcl it's okay to override a function, so it always gives warnings right now


(defun nn () "return newline" ; {{{
"
"
) ; }}}

(defun open-string() "create string with fill pointer" ; {{{
	(make-array '(0) :element-type 'base-char :fill-pointer 0 :adjustable t)
) ; }}}

(defun first-arg (args) "return $1 command line arg or nil" ; {{{
	(cond
	((or (null args) (< (length args) 1))
		nil)
	(t (nth 1 args))
)) ; }}}

(defstruct style-rule
	selector
	properties
	comment  ; an 'anything' argument for use in things like bopwiki
)

;;; }}}


;;; stylesheet generator {{{

;; example data {{{
;; each stylesheet should basically have its own definition of these
;; which should be passed in as arguments to style-from-arg

(defun example-items () "dummy list for testing" ; {{{
	(list "https://www.youtube.com/watch?v=example"
	"https://www.youtube.com/watch?v=example2")
) ; }}}

(defun shorten-url (url) "return minimal version of url" ; {{{
	url
) ; }}}

(defun url-to-style (url) "return all selectors needed for each url" ; {{{
	(format nil
		", [href*=\"~a\"] ~~ .something"  ; example: , [href*=".+"] ~ .something
		url
)) ; }}}

(defun default-style-end () "body of style rule to \"display\" selectors" ; {{{
	"{ display: inline-block; }"
) ; }}}

;; }}}


(defun split-lines (text) ; {{{
	(if (listp text)
		text
		(uiop:split-string text))
) ; }}}

(defun load-items (args  &key url-list) ; {{{
"load data from file if given, or example function"
	(let ((frst
		(unless (null args)  (first-arg args))
	))
	
	(cond
		((listp url-list)  url-list)
		((stringp url-list)  (split-lines url-list))
		((not (null url-list))  (uiop:read-file-lines url-list))
		((not (null frst))  (uiop:read-file-lines frst))
		(t (example-items)))
)) ; }}}


(defun stylesheet ; output full stylesheet {{{
	(watched-list  ; list of lines
		&key shorten-url selector style-end separator)
"output full stylesheet"
	
	(let (brief
		;; if style-end string wasn't passed, fill it in with default one
		(ending 
			(if (null style-end)
				(default-style-end) style-end)
		)
		(result (open-string)) (url-to-style selector)
		(separator-str
			(if (null separator) 
				(format nil "~%") separator))
	)
	
	(with-output-to-string (s result)
		;; add dummy selector to allow commas at beginning
		(format s "dummy~%")
		;; add style rule for each url
		(dolist (item watched-list)
			(cond
			;; discard comment lines starting with "#"
			;; i tried to word this as an 'unless' but it just wouldn't work
			((or  (< (length item) 1)  (char= (elt item 0) #\#)  ) '())
			(t
				(setq brief (funcall shorten-url item))
				(format s "~A~a" (funcall url-to-style brief) separator-str)
		)))
		;; add actual rule's style 'traits' to activate them
		(format s "~%~a~%" ending)
	)
	result
)) ; }}}

(defun style-from-arg ; {{{
	(&key shorten-url selector style-end separator selector-list url-list)
""
	(let ((result (open-string)) (style-separator "")
		(args (uiop:command-line-arguments))
	)
	
	(when (null selector-list)
		(setq
			style-separator (format t "~%~%")
			selector-list
			(list
				(ytp:make-style-rule :selector selector :properties style-end))
	))
	
	style-separator  ;; TODO: fix
	
	(with-output-to-string (s result)
	(dolist (rule selector-list)
		(format s "~a"
			(stylesheet
				(load-items args :url-list url-list)
				
				:shorten-url shorten-url
				:selector (style-rule-selector rule)
				:style-end (style-rule-properties rule)
				:separator separator))
	))
	
	result
)) ; }}}

;;; }}}
