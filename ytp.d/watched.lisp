;;; watched.lisp - functions specific to video stylesheets
(defpackage :ytp-stylesheet-videos
	(:use :common-lisp)
	;;(:local-nicknames (:ytp :ytp))
	(:export  #:shorten-url)
)
(in-package :ytp-stylesheet-videos)


(defun shorten-url (url) ; return minimal version of url {{{
"return minimal version of url"
	(let ((match (ytp:open-string)) (len (- (length url) 1)) vee stop)
	
	;; output match to "match"
	(with-output-to-string (s match)
	(let ((*standard-output* s) chr next)
	;; look through string to find v=
	(loop for  i from 0 to len  while (null stop)  do
		(setq chr (elt url i)) 
		(setq next
			(when (< i len)
				(elt url (+ i 1))
				))
		
		;; collect v= parameter
		(cond
			;; start collecting match at v=
			((and (equal chr #\v) (equal next #\=))
				(setq vee t))
			;; stop at either & or end
			((equal chr #\&)
				(setq vee nil) (setq stop t))
			
			;; I could use regexes... but had trouble up to right now getting sbcl to load dependencies.
			;; LATER: consider how to handle peertube urls
		)
		
		(unless (null vee)
			(format s "~a" chr))
	)))
	;; return either the match or unedited string
	(if (or (null match) (equal match ""))  url match)
)) ; }}}
