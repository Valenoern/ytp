(defsystem "ytp"
	; jsown
	:depends-on ("uiop")
	:components (
		(:file "ytp")
		(:file "watched" :depends-on ("ytp"))  ; :ytp-stylesheet-videos
))
