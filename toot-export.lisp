;;; bop-toots.lisp - test of converting activitypub objects to bop pages {{{
(asdf:load-system "bop-toots")
;(asdf:load-system "bop-debug")

;; regex information used to fix urls in posts
;; ppcre supposedly performs a little better if you reuse regexes than keep creating them
;(defparameter *status-regex* nil)

(defpackage :ytp-neo-main
	(:use :common-lisp :uiop :bop-toots
		;:bop-debug
	)
	;(:export  #:bar)
)
(in-package :ytp-neo-main)

;;; }}}



(defun main () ; {{{
;; this program should be called like:  bop-toots.lisp <OUTBOX-JSON>
;; where <OUTBOX-JSON> is a path to a mastodon export
	(let ((args (uiop:command-line-arguments)) input-file toots
		(status-regex (status-scanner-mastodon))
	)
	(setq input-file (nth 1 args))  ; get $1 argument
	
	;; if no filename was given as $1, output demo object
	(if (not (uiop:file-exists-p input-file))  (parse-example-toot  :scanner status-regex)
	;; otherwise try to parse the file
	(progn
		;; as the input file and parsed data are big,
		;; attempt to not to keep them each in memory longer than needed
		(let (parsed)  ; bind PARSED only long enough to render
			(let (input)  ; bind INPUT only long enough to parse
				(setq input (uiop:read-file-string input-file))
				(setq parsed (jsown:parse input "orderedItems"))
			)
			(setq toots (simple-render (collection-to-list parsed  :scanner status-regex)))
		)
		
		(princ toots)
	))
)) ; }}}

(main)
