;;; stylesheets to mark "watched" and "hidden" videos
;; these can be used as a bop publish format - no docs currently
(asdf:load-system "ytp-examples")

(defpackage :ytp-example-commands
	(:use :common-lisp)
	;; uiop
	(:local-nicknames 
		(:watched :ytp-example-watched)
		(:toots :ytp-example-toots)
	)
)
(in-package :ytp-example-commands)


(defun script-args (args) ; {{{
"subset arguments if script filename is first argument"
	(let ((frst-arg (uiop:ensure-pathname (nth 0 args))))
	
	(when (equal (pathname-name frst-arg) "yt-style")
		(setq args (subseq args 1)))
	
	args
)) ; }}}


;; main
(let (frst snd
	(args (script-args (uiop:command-line-arguments)))
	)
	
	(setq
		frst (nth 0 args)
		snd (uiop:ensure-pathname (nth 1 args)))
	
	(princ
	(cond
		((equal frst "watched-vids") (watched:watched-vids :url-list snd))
		((equal frst "hidden-vids") (watched:hidden-vids :url-list snd))
		
		((equal frst "archived-toots") (toots:archived-toots :url-list snd  :preamble t))
		((equal frst "archived-toots-basic") (toots:archived-toots :url-list snd))
	))
)
