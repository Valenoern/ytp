(defpackage :bop-debug
	(:use :common-lisp :uiop)
	(:export
		#:printl
		#:debug-hash
))
(in-package :bop-debug)


(defun printl (&rest args) "shorthand for (print (list)) to decrease nesting" ; {{{
	(print args)
) ; }}}

(defun debug-hash(table) "print contents of a hash table" ; {{{
	;; https://www.tutorialspoint.com/lisp/lisp_hash_table.htm
	(maphash #'(lambda (k v) (format t "~a => ~a~%" k v)) table)
) ; }}}
