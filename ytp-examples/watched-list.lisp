;; watched-list.lisp
(defpackage :ytp-example-watched
	(:use :common-lisp)
	(:local-nicknames
		(:ytp-vid :ytp-stylesheet-videos)
	)
	(:export
		#:watched-vids #:hidden-vids
))
(in-package :ytp-example-watched)


(defun watched-vids (&key url-list) ; {{{
"highlight watched vids"
	(ytp:style-from-arg
		:url-list url-list
		:shorten-url (function ytp-vid:shorten-url)
		
		:selector-list
		(list
			(ytp:make-style-rule
				:selector
				(lambda (url)
					(format nil
						", a[href*=\"~a\"], a[href*=\"~a\"] *"  url url))
				:properties
					"{ background: darkcyan; outline: 4px solid darkcyan; }"))
)) ; }}}

(defun hidden-vids (&key url-list) ; {{{
"hide recommended and listed vids"
	(ytp:style-from-arg
		:separator ""
		:url-list url-list
		:shorten-url (function ytp-vid:shorten-url)
		
		:selector-list
		(list
			(ytp:make-style-rule  ; first rule
				:selector
				(lambda (url)
				(format nil
					", .yt-simple-endpoint[href*=\"~a\"]"  url))
				:properties
					"{ background: grey; }")
					
			(ytp:make-style-rule  ; second rule
				:selector
				(lambda (url)
				(format nil
					", .yt-simple-endpoint[href*=\"~a\"] *"  url))
				:properties
					"{ opacity: 0; }"))
)) ; }}}
