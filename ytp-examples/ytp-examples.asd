(defsystem "ytp-examples"
	:depends-on ("ytp" "uiop")
	:components (
		(:file "watched-list")  ; :ytp-example-watched
		(:file "toots")  ; :ytp-example-toots
))
