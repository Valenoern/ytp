;; toots.lisp - stylesheet to mark mastodon toots mirrored into a bopwiki, etc {{{
(defpackage :ytp-example-toots
	(:use :common-lisp)
	;;(:local-nicknames
	;;	(:ytp-vid :ytp-stylesheet-videos)
	;;)
	(:export
		;; archived-toots-preamble
		#:archived-toots
))
(in-package :ytp-example-toots)
;; }}}


(defun archived-toots-preamble () ; {{{
"/* characteristics of archived post mark, 1. highlighted status and 2. unhighlighted */
.detailed-status__display-name
{ position: relative; }
.detailed-status__display-avatar + .display-name:after,  .status__relative-time:before
{ font-size: 1.8em;  position: absolute; bottom: 6%; right: 3%; }
[href*=\"/favourites\"]:after
{ font-size: 1.5em;  position: relative; right: -0.8em; vertical-align: middle; }

/* archived post mark content */
.status__relative-time:before, [href*=\"/favourites\"]:after
{ content: \"❈\"; display: none; }"
) ; }}}

(defun archived-toots (&key url-list preamble) ; {{{
"mark archived toots"
	(let ((preamble-text
		(cond
			((null preamble) nil)
			((equal preamble t)
				(format nil "~A~%~%~%" (archived-toots-preamble)))
			;; if a specific preamble was given, use it
			(t preamble)
		)
	))
	
	(format nil "~a~a"
		preamble-text
		(ytp:style-from-arg
			:url-list url-list
			:shorten-url
				(lambda (url)  (uiop:frob-substrings url (list "http:/" "https:/")))
		
			:selector-list
			(list
			(ytp:make-style-rule
				:selector
				(lambda (url)
					(format nil
					", .status__relative-time[href*=\"~a\"]:before, [href*=\"~a\"] ~~ [href*=\"favourites\"]:after"
						url url))
				:properties
					"{ display: inline-block; }"))
	))
)) ; }}}


;; style-end = the default one in ytp.lisp

