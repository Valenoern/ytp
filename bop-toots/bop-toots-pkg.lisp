(uiop:define-package :bop-toots
	;(:nicknames :my-lib-interface)
	;(:use :closer-common-lisp)
	(:mix :common-lisp :uiop)
	(:use-reexport :bop-toots-core :bop-toots-collection)
)
