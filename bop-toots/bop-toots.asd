(defsystem "bop-toots"
	:depends-on ("uiop" "jsown" "cl-ppcre")
	:components (
		(:file "toots")
		(:file "collection" :depends-on ("toots"))
		(:file "bop-toots-pkg" :depends-on ("toots" "collection"))
))
;; use the whole system in a package with - :use bop-toots
