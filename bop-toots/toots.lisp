(defpackage :bop-toots-core
	(:use :common-lisp :uiop)
	(:export
		;; not exported:
		;; structs , example-toot
		
		#:status-scanner-mastodon #:status-url
		#:toot-to-bop #:toot-from-activity
		
		#:parse-example-toot
))
(in-package :bop-toots-core)

(defstruct toot   url date content parent media)
(defstruct attachment  ext url caption)


;;; canonical post urls - status-scanner- , status-url {{{

(defun status-scanner-mastodon () ; mastodon url regex {{{
"Expression to convert the url of a mastodon toot (string) into the canonical version used in the mastodon web UI, for example:
https://instance.name/users/exampleuser/statuses/391191437864380884 -> https://instance.name/@exampleuser/391191437864380884"
	(list (ppcre:create-scanner "(.+)\/users\/(.+)\/statuses/(.+)") "\\1/@\\2/\\3")
) ; }}}

(defun status-url (url regex) ; fix url to e.g. '@' version {{{
"Convert a post URL (string) into its canonical version.
REGEX should be a two-item list containing the search and replace strings, provided by a status-scanner- function - at this time only there is only status-scanner-mastodon to convert mastodon urls."
	;; this list should have been created by a status-scanner- function
	(let ((search-string (nth 0 regex)) (replace-string (nth 1 regex)))
	(ppcre:regex-replace search-string url replace-string)
)) ; }}}

;;; }}}


(defun toot-from-activity (activity  &key scanner) ; filter json-ld toot into toot struct {{{
"Filter a toot ACTIVITY in json-ld format down to only relevant properties, returning a TOOT struct.
The regular expression used to correct urls may be passed in with :scanner"
	(let (attach-list url  ; id date content attachment parent {{{
		(id (jsown:filter activity "object" "id"))
		(date (jsown:filter activity "object" "published"))
		(content (jsown:filter activity "object" "content"))
		(attachment (jsown:filter activity "object" "attachment"))
		(parent (jsown:filter activity "object" "inReplyTo"))
		;(regex *status-regex*)
		(regex scanner)
	) ; }}}
	;(when (null regex)  (setq regex scanner))
	
	;; process attachments
	(dolist (attach attachment) ; {{{
		(let ((url (jsown:filter attach "url")) (ext (jsown:filter attach "mediaType"))
			(caption (jsown:filter attach "name"))
		)
		(setq attach-list (cons (make-attachment :ext ext :url url :caption caption) attach-list))
	)) ; }}}
	
	;; fix urls if necessary
	(setq url (status-url id regex))
	(unless (null parent)
		(setq parent (status-url parent regex)))
	
	;; return simplified toot struct
	(make-toot  :url url  :date date  :content content  :media attach-list  :parent parent)
)) ; }}}

(defun toot-to-bop (toot) ; render toot as standard bop entry {{{
"Render a TOOT in the form of a TOOT struct into a bop entry file as a single string"
	(let ((content (toot-content toot)) (media-str "") meta-lines ; url date media {{{
		(url (format nil "== ~a~%" (toot-url toot)))
		(date (format nil "; posted ~a" (toot-date toot)))
		(parent (toot-parent toot))
	) ; }}}
	
	;; print parent context link only if it actually has data
	(setq parent
		(if (null parent)  ""  (format nil "; <= ~a~%" parent)))
	
	;; if there are any attachments flatten each one
	(let ((media (toot-media toot)) description)
	(dolist (attach media)
		;; get line to print below attachment url - either description or "no description"
		(let ((caption (attachment-caption attach)))
		(setq description
			(if (null caption)  "no description"  (format nil "desc: ~a" caption))
		))
		;; add attachment onto string of attachments
		(setq media-str (format nil "~a~a" media-str ;new-attachment below
			(format nil "; @ ~a~%;   ~a~%" (attachment-url attach) description)
		))
	))
	
	;; flatten media lines
	(setq meta-lines (format nil "~a~a~a~a" media-str url parent date))
	
	;; add linebreaks between content and meta lines
	(setq content (format nil "~a~%~%" content))
	(format nil "~a~a"  content meta-lines)
)) ; }}}


;;; example toot data - example-toot , parse-example-toot {{{

(defun example-toot (&key media) "example toot represented as a JSON string" ; {{{
	(let (  ; template attach-nil attach-t {{{
		(template "{ \"@context\": ~a, \"type\": \"Create\", \"actor\": \"https://instance.name/users/exampleuser\", \"published\": \"2020-08-25T10:35:39Z\", \"to\": [ \"https://instance.name/users/exampleuser/followers\" ], \"cc\": [ \"https://www.w3.org/ns/activitystreams#Public\" ], \"id\": \"https://instance.name/users/exampleuser/statuses/365002096534761105/activity\",    \"object\": { \"type\": \"Note\", \"summary\": null, \"published\": \"2020-08-25T10:35:39Z\", \"attributedTo\": \"https://instance.name/users/exampleuser\", \"to\": [ \"https://instance.name/users/exampleuser/followers\" ], \"cc\": [ \"https://www.w3.org/ns/activitystreams#Public\" ], \"sensitive\": false, \"content\": \"<p>test content</p>\", \"contentMap\": { \"en\": \"<p>test content</p>\" }, \"tag\": [], \"id\": \"https://instance.name/users/exampleuser/statuses/365002096534761105\", \"inReplyTo\": \"https://instance.name/users/exampleuser/statuses/363477584492732244\", \"url\": \"https://instance.name/@exampleuser/365002096534761105\", \"atomUri\": \"https://instance.name/users/exampleuser/statuses/365002096534761105\", \"inReplyToAtomUri\": \"https://instance.name/users/exampleuser/statuses/363477584492732244\", \"conversation\": \"tag:instance.name,2020-08-25:objectId=1598380539:objectType=Conversation\", \"attachment\": ~a,  \"replies\": ~a }}")
		
		attach  ; context attach-nil attach-t replies {{{
		(context "[ \"https://www.w3.org/ns/activitystreams\", { \"ostatus\": \"http://ostatus.org#\", \"atomUri\": \"ostatus:atomUri\", \"inReplyToAtomUri\": \"ostatus:inReplyToAtomUri\", \"conversation\": \"ostatus:conversation\", \"sensitive\": \"as:sensitive\", \"toot\": \"http://joinmastodon.org/ns#\", \"votersCount\": \"toot:votersCount\" } ]")
		
		(attach-nil "[]")
		(attach-t "[{\"type\":\"Document\",\"mediaType\":\"image/png\",\"url\":\"/media/media_attachments/files/915/765/976/436/833/877/original/vudz9dgle0gh8qt2.png\",\"name\":\"description of image\",\"blurhash\":\"scjJkHCyDM6GwmwIybkmXFDUpTe3ZICHEJ1g\",\"focalPoint\":[0.0,0.0]}]")
		
		(replies "{ \"type\": \"Collection\", \"id\": \"https://instance.name/users/exampleuser/statuses/365002096534761105/replies\", \"first\": { \"type\": \"CollectionPage\", \"next\": \"https://instance.name/users/exampleuser/statuses/365002096534761105/replies?only_other_accounts=true&page=true\", \"partOf\": \"https://instance.name/users/exampleuser/statuses/365002096534761105/replies\", \"items\": [] }}")
		; }}}
	) ; }}}
	
	;; add extra things if requested
	(setq attach (if (null media)  attach-nil attach-t))  ; add attach if :media t
	
	(format nil template context attach replies)
)) ; }}}

(defun parse-example-toot (&key scanner) ; simple example using EXAMPLE-TOOT {{{
	(let ((example (example-toot :media t)) parsed bopstruct)
	
	;; parse json
	(setq parsed (jsown:parse example "type" "object"))
	(setq bopstruct (toot-from-activity parsed  :scanner scanner))
	
	(princ (toot-to-bop bopstruct))
	
	#| output:
<p>test content</p>

; @ /media/media_attachments/files/915/765/976/436/833/877/original/vudz9dgle0gh8qt2.png
;   desc: description of image
== https://instance.name/users/exampleuser/statuses/365002096534761105
; posted 2020-08-25T10:35:39Z
	|#
)) ; }}}

;;; }}}

