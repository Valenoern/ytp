(defpackage :bop-toots-collection
	(:use :common-lisp :uiop :bop-toots-core)
	(:export
		;; not exported: none
		
		#:collection-to-list
		#:simple-render
))
(in-package :bop-toots-collection)


(defun collection-to-list (collection &key oldest scanner) ; collection object to toot structs {{{
	(let ((items (jsown:filter collection "orderedItems")) toots)
	
	(dolist (item items)
		(let ((item-type (jsown:filter item "type")))
		;; add only 'original' toots which are classified as Create, and not boosts (Announce)
		(when (equal item-type "Create")
			(setq toots (cons (toot-from-activity item  :scanner scanner) toots)))
	))
	;; if passed :oldest t, return toots from oldest to newest, otherwise newest first
	;; LATER: allow passing this on command line somehow
	(if (null oldest)  toots (nreverse toots))
)) ; }}}

(defun simple-render (toots) ; render all toots to one string, for diff purposes etc {{{
	(let ((result-str "") (full-divider (format nil "~%~%---~%~%")))
	
	(loop for i  from 0 to (- (length toots) 1) do
		(let ((toot (nth i toots)) toot-str (divider ""))
		(setq toot-str (toot-to-bop toot))
		(when (> i 0)  (setq divider full-divider))  ; add divider only between toots
		(setq result-str (format nil "~a~a~a" result-str divider toot-str))
	))
	result-str
	
	;; when testing this I found out joining strings with uiop:strcat was /vastly/ slower than format for some reason
	;; so i will not be using that any more.
)) ; }}}
